package com.wyg.drools_wb_quick_start;

import org.drools.core.io.impl.UrlResource;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.KieRepository;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.InputStream;

@SpringBootTest
class DroolsWbQuickStartApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void testDrools() throws IOException {
        // 通过此URL可以访问到maven仓库中的jar包
        // URL地址构成: http://ip:tomcat端口号/workbench工程名/maven2/坐标/版本号/xxx.jar
        String url = "http://localhost:8050/kie-drools-wb/maven2/com/wyg/drools_wb_quick_start/1.0.0/drools_wb_quick_start-1.0.0.jar";
        KieServices kieServices = KieServices.Factory.get();

        // 通过resource资源加载jar包
        UrlResource resource = (UrlResource) kieServices.getResources().newUrlResource(url);
        // 通过workbench提供的服务来访问maven仓库中的jar包资源,需要先进行workbench的认证
        resource.setUsername("kie");
        resource.setPassword("kie");
        resource.setBasicAuthentication("enabled");

        // 将资源转换为输入流,通过输入流可以读取jar包数据
        InputStream inputStream = resource.getInputStream();

        // 创建仓库对象,仓库对象中保存drools的规则信息
        KieRepository repository = kieServices.getRepository();

        // 通过输入流读取maven仓库中的jar包数据,包装成kiemodule模块添加到仓库中
        KieModule kieModule = repository.addKieModule(kieServices.getResources().newInputStreamResource(inputStream));

        // 基于kiemodule模块创建容器对象,从容器中可以获取session会话
        KieContainer kieContainer = kieServices.newKieContainer(kieModule.getReleaseId());
        KieSession session = kieContainer.newKieSession();

        Student student = new Student();
        student.setAge(16);
        session.insert(student);
        session.fireAllRules();
        session.dispose();
    }
}
