package com.wyg.drools_wb_quick_start;

import lombok.Data;

/**
 * 实体类,对应workbench中drools的实体文件
 * 这里必须和workbench中的实体类一致:包名一致,类名一致,属性一致
 */
@Data
public class Student {
    private String name;
    private int age;
    private int id;
}
