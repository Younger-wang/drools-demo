package com.wyg.drools_wb_quick_start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroolsWbQuickStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(DroolsWbQuickStartApplication.class, args);
    }

}
