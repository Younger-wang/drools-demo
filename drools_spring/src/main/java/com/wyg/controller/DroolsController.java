package com.wyg.controller;

import com.wyg.service.api.DroolsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/drools")
public class DroolsController {

    @Autowired
    private DroolsService droolsService;

    @RequestMapping("/get")
    public String getDroolsResult() {
        return droolsService.executeDrools();
    }
}
