package com.wyg.service.impl;

import com.wyg.service.api.DroolsService;
import org.kie.api.KieBase;
import org.kie.api.cdi.KBase;
import org.kie.api.runtime.KieSession;
import org.springframework.stereotype.Service;

@Service
public class DroolsServiceImpl implements DroolsService {

    @KBase
    KieBase kieBase;

    @Override
    public String executeDrools() {
        KieSession session = kieBase.newKieSession();
        session.fireAllRules();
        session.dispose();
        return "success";
    }
}
