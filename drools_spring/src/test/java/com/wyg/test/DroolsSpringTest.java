package com.wyg.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieBase;
import org.kie.api.cdi.KBase;
import org.kie.api.runtime.KieSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mvc.xml")
public class DroolsSpringTest {

    @KBase("kbase")
    private KieBase kieBase;

    @Test
    public void testHelloWorld() {
        KieSession session = kieBase.newKieSession();
        session.fireAllRules();
        session.dispose();
    }
}
