package com.wyg.drools.springboot.drools.service.impl;

import com.wyg.drools.springboot.drools.entity.Calculation;
import com.wyg.drools.springboot.drools.service.api.RuleService;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RuleServiceImpl implements RuleService {

    @Autowired
    private KieBase kieBase;

    @Override
    public void defaultRule() {
        KieSession session = kieBase.newKieSession();
        session.fireAllRules();
        session.dispose();
    }

    @Override
    public Calculation calculate(Calculation calculation) {
        KieSession session = kieBase.newKieSession();
        session.insert(calculation);
        session.fireAllRules();
        session.dispose();
        return calculation;
    }
}
