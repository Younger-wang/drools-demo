package com.wyg.drools.springboot.drools.controller;

import com.wyg.drools.springboot.drools.entity.Calculation;
import com.wyg.drools.springboot.drools.service.api.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/drools")
public class RuleController {

    @Autowired
    private RuleService ruleService;

    @GetMapping("/defaultRule")
    public String defaultRule() {
        ruleService.defaultRule();
        return "success";
    }

    /**
     * 个人所得税计算器
     *
     * @param wage 税前工资
     * @return Calculation
     */
    @GetMapping("/calculate")
    public Calculation calculate(double wage) {
        Calculation calculation = new Calculation();
        calculation.setWage(wage);
        ruleService.calculate(calculation);
        System.out.println(calculation);
        return calculation;
    }
}
