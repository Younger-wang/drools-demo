package com.wyg.drools.springboot.drools.service.api;

import com.wyg.drools.springboot.drools.entity.Calculation;

public interface RuleService {

    /**
     * 获取默认规则文件demo
     */
    void defaultRule();

    /**
     * 计算个人所得税
     *
     * @param calculation calculation
     * @return Calculation
     */
    Calculation calculate(Calculation calculation);

}
