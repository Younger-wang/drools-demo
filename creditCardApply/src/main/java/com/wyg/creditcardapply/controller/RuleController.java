package com.wyg.creditcardapply.controller;

import com.wyg.creditcardapply.service.RuleService;
import com.wyg.creditcardapply.vo.CreditCardApplyInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rule")
public class RuleController {
    @Autowired
    private RuleService ruleService;

    @PostMapping("/creditCardApply")
    public CreditCardApplyInfo creditCardApply(@RequestBody CreditCardApplyInfo creditCardApplyInfo) {
        ruleService.creditCardApply(creditCardApplyInfo);
        return creditCardApplyInfo;
    }
}
