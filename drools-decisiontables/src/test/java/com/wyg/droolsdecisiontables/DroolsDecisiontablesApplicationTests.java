package com.wyg.droolsdecisiontables;

import com.wyg.droolsdecisiontables.vo.PersonInfoVo;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.junit.jupiter.api.Test;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class DroolsDecisiontablesApplicationTests {

    @Test
    void contextLoads() {
        System.out.println("test");
    }

    @Test
    void Test1() throws FileNotFoundException {
        String realPath = "classpath:static/决策表.xls";
        File file = ResourceUtils.getFile(realPath);
        FileInputStream fis = new FileInputStream(file);
        SpreadsheetCompiler sc = new SpreadsheetCompiler();
        // 决策表转换为drl字符串
        String drl = sc.compile(fis, InputType.XLS);
        System.out.println(drl);
        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);
        KieSession session = kieHelper.build().newKieSession();
        List<String> list = new ArrayList<>();
        session.setGlobal("listRules", list);
        PersonInfoVo person = PersonInfoVo.builder().sex("男").age(35).salary(1000).build();
        session.getAgenda().getAgendaGroup("sign").setFocus();
        session.insert(person);
        session.fireAllRules();
        for (String s : list) {
            System.out.println(s);
        }
        session.dispose();
    }

    public static void main(String[] args) throws FileNotFoundException {
        String realPath = ResourceUtils.CLASSPATH_URL_PREFIX + "static/决策表.xls";
        File file = ResourceUtils.getFile(realPath);
        FileInputStream fis = new FileInputStream(file);
        SpreadsheetCompiler sc = new SpreadsheetCompiler();
        // 决策表转换为drl字符串
        String drl = sc.compile(fis, InputType.XLS);
        System.out.println(drl);
        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);
        KieSession session = kieHelper.build().newKieSession();
        session.fireAllRules();
        session.dispose();
    }
}
