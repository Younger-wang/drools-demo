package com.wyg.droolsdecisiontables.vo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PersonInfoVo {
    private String sex;
    private int age;
    private double salary;
}
