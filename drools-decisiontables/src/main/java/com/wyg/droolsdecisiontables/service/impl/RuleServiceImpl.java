package com.wyg.droolsdecisiontables.service.impl;

import com.wyg.droolsdecisiontables.service.RuleService;
import com.wyg.droolsdecisiontables.utils.KieSessionUtils;
import com.wyg.droolsdecisiontables.vo.InsuranceInfoVo;
import org.kie.api.runtime.KieSession;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RuleServiceImpl implements RuleService {

    @Override
    public List<String> insuranceInfoCheck(InsuranceInfoVo insuranceInfo) throws Exception {
        KieSession kieSession = KieSessionUtils.getKieSessionFromXLS("classpath:static/保险单.xls");
        kieSession.getAgenda().getAgendaGroup("sign").setFocus();
        kieSession.insert(insuranceInfo);
        List<String> list = new ArrayList<>();
        kieSession.setGlobal("listRules", list);
        kieSession.fireAllRules();
        return list;
    }
}
