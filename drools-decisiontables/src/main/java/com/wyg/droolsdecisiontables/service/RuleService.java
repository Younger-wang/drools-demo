package com.wyg.droolsdecisiontables.service;

import com.wyg.droolsdecisiontables.vo.InsuranceInfoVo;

import java.util.List;

public interface RuleService {
    List<String> insuranceInfoCheck(InsuranceInfoVo insuranceInfo) throws Exception;
}
