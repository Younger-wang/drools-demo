package com.wyg.droolsdecisiontables.utils;

import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class KieSessionUtils {

    /**
     * xls转drl
     * @param path 文件路径
     * @return drl
     * @throws FileNotFoundException FileNotFoundException
     */
    public static String getDrl(String path) throws FileNotFoundException {
//        File file = new File(path);
        File file = ResourceUtils.getFile(path);
        FileInputStream fis = new FileInputStream(file);
        SpreadsheetCompiler compiler = new SpreadsheetCompiler();
        String drl = compiler.compile(fis, InputType.XLS);
//        System.out.println(drl);
        return drl;
    }

    /**
     * drl转session
     * @param drl drl
     * @return KieSession
     */
    public static KieSession createKieSessionFromDRL(String drl) {
        KieHelper helper = new KieHelper();
        helper.addContent(drl, ResourceType.DRL);
        Results result = helper.verify();
        if (result.hasMessages(Message.Level.WARNING, Message.Level.ERROR)) {
            List<Message> messages = result.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
                System.out.println("Error: "+message.getText());
            }
            // throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }
        return helper.build().newKieSession();
    }

    /**
     * xls转session
     * @param path 文件路径
     * @return KieSession
     * @throws FileNotFoundException FileNotFoundException
     */
    public static KieSession getKieSessionFromXLS(String path) throws FileNotFoundException {
        return createKieSessionFromDRL(getDrl(path));
    }

}
