package com.wyg.droolsdecisiontables;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroolsDecisiontablesApplication {

    public static void main(String[] args) {
        SpringApplication.run(DroolsDecisiontablesApplication.class, args);
    }

}
