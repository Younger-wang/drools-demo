package com.wyg.drools.entity;

/**
 *
 */
public class Order {
    /**
     * 订单原始价格，即优惠前价格
     */
    private double originalPrice;

    /**
     * 订单真实价格，即优惠后价格
     */
    private double realPrice;

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(double realPrice) {
        this.realPrice = realPrice;
    }

    @Override
    public String toString() {
        return "Order{" +
                "originalPrice=" + originalPrice +
                ", realPrice=" + realPrice +
                '}';
    }
}
