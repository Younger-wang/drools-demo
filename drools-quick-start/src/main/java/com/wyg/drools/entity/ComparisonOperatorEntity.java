package com.wyg.drools.entity;

import java.util.List;

/**
 * 用于测试比较操作符
 */
public class ComparisonOperatorEntity {
    private String name;

    private List<String> list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "ComparisonOperatorEntity{" +
                "name='" + name + '\'' +
                ", list=" + list +
                '}';
    }
}
