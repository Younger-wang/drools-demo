package com.wyg.drools.test;

import org.junit.Before;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

public class BaseTest {

    private KieSession kieSession;

    private KieBase kieBase;

    /**
     * 执行规则引擎
     *
     * @param object Fact对象
     * @param agendaFilter 议程过滤器
     */
    public void executeRule(Object object, AgendaFilter agendaFilter) {
        kieSession.insert(object);
        // 避免程序立即结束，启动规则引擎进行规则匹配，直到调用halt方法才结束规则引擎
        new Thread(() -> kieSession.fireUntilHalt(agendaFilter)).start();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("result is : " + object);
        kieSession.halt(); // 结束规则引擎
        kieSession.dispose();
    }

    public KieSession getKieSession() {
        return kieSession;
    }

    public KieBase getKieBase() {
        return kieBase;
    }

    /**
     * 重新初始化kieSession
     */
    public void restartSession() {
        kieSession = getKieBase().newKieSession();
    }

    /**
     * 初始化规则引擎
     */
    @Before
    public void initKieSession() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer container = kieServices.getKieClasspathContainer();
        // 设置日期格式,要在获取session之前设置
        System.setProperty("drools.dateformat","yyyy-MM-dd");
        kieSession = container.newKieSession();
        kieBase = container.getKieBase();
    }
}
