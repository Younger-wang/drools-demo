package com.wyg.drools.test;


import com.wyg.drools.entity.ComparisonOperatorEntity;
import com.wyg.drools.entity.Order;
import com.wyg.drools.entity.Student;
import org.drools.core.base.RuleNameEqualsAgendaFilter;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DroolsTest extends BaseTest {


    /**
     * 测试图书优惠-基础语法
     */
    @Test
    public void testBook() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer container = kieServices.getKieClasspathContainer();

        // 会话对象，用于和规则引擎交互
        KieSession kieSession = container.newKieSession("ksession-rule");

        // 构造订单对象，设置原始价格，由规则引擎根据优惠规则计算优惠后的价格
        Order order = new Order();
        order.setOriginalPrice(180);

        // 将数据提供给规则引擎，规则引擎会根据提供的数据进行规则匹配
        kieSession.insert(order);

        // 激活规则引擎，如果配置成功则执行规则
        // 参数3代表最多激活3个规则
        kieSession.fireAllRules(3);

        // 关闭会话
        kieSession.dispose();
        System.out.println("优惠前原始价格：" + order.getOriginalPrice() +
                "，优惠后价格：" + order.getRealPrice());
    }

    /**
     * 测试比较操作符
     */
    @Test
    public void testOperator() {
        ComparisonOperatorEntity entity = new ComparisonOperatorEntity();
        entity.setList(Collections.singletonList("张三"));
        entity.setName("张三1");
        executeRule(entity, null);
    }

    /**
     * 测试指定规则
     */
    @Test
    public void testAppointRule() {
        ComparisonOperatorEntity entity = new ComparisonOperatorEntity();
        entity.setList(Collections.singletonList("张三"));
        entity.setName("张三1");
        // 激活规则，由Drools框架自动进行规则匹配，如果规则匹配成功，则执行当前规则
        // 使用框架提供的规则过滤器执行指定规则
        executeRule(entity, new RuleNameEqualsAgendaFilter("rule_comparisonOperator_not_memberOf"));
    }

    /**
     * 测试内置方法，update，insert，retract
     */
    @Test
    public void testStudent() {
        Student student = new Student(10);
        executeRule(student, null);
    }

    /**
     * 测试规则属性：enabled，是否启用规则
     */
    @Test
    public void testAttribute() {
        KieSession kieSession = getKieSession();
        // 手动激活议程分组,测试agenda-group属性
        kieSession.getAgenda().getAgendaGroup("my-agenda").setFocus();
        executeRule(new Student(30), null);
    }

    /**
     * 测试高级属性 global
     */
    @Test
    public void testAdvanceKeyGlobal() {
        KieSession session = getKieSession();
        // 设置全局变量,变量名称必须和规则文件中定义的变量名一致
        Integer count = 5;
        List<String> aList = new ArrayList<>();
        Student aStudent = new Student();
        aStudent.setAge(50);
        session.setGlobal("count", count);
        session.setGlobal("aList", aList);
        session.setGlobal("aStudent", aStudent);
        RuleNameStartsWithAgendaFilter startsWithAgendaFilter = new RuleNameStartsWithAgendaFilter("rule_advanceKey_global");
        executeRule(null, startsWithAgendaFilter);
        System.out.println("规则引擎执行完成 count = " + count+
                "\naList = " + Arrays.toString(aList.toArray()) +
                "\naStudent = " + aStudent);
    }

    /**
     * 测试高级属性 query
     */
    @Test
    public void testAdvanceKeyQuery() {
        // query符合条件的Fact对象(无参)
        KieSession session = getKieBase().newKieSession();
        Student aStudent = new Student();
        aStudent.setAge(50);
        // 给工作内容中插入student
        session.insert(aStudent);
        // 查询符合条件的Fact对象,无法查出global的Fact对象
        QueryResults results = session.getQueryResults("rule_advanceKey_query_noParameter");
        System.out.println("=================================查询query,无参");
        for (QueryResultsRow result : results) {
            System.out.println(result.get("$s"));
        }

        // query符合条件的Fact对象(带参)
        aStudent.setName("李四");
        // 查询符合条件的Fact对象
        QueryResults result1 = session.getQueryResults("rule_advanceKey_query_parameter", "李四");
        System.out.println("=================================查询query,有参");
        for (QueryResultsRow result : result1) {
            System.out.println(result.get("$s"));
        }
    }

    /**
     * 测试高级属性 Function
     */
    @Test
    public void testAdvanceKeyFunction() {
        Student student = new Student();
        student.setName("王五");
        executeRule(student, new RuleNameStartsWithAgendaFilter("rule_advanceKey_function"));
    }

    /**
     * 测试global的线程安全,非线程安全的.
     *
     * @throws InterruptedException ex
     */
    @Test
    public void testDroolsGlobalSyn() throws InterruptedException {
        List<String> aList = new ArrayList<>();
        new Thread(() -> {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < 100 ; i++) {
                Student student = new Student();
                student.setName("thread2");
                KieSession session = getKieBase().newKieSession();
                session.insert(student);
                session.setGlobal("aList", aList);
                session.fireAllRules(new RuleNameEqualsAgendaFilter("rule_advanceKey_global_add_syn"));
                session.dispose();
            }
            System.out.println(Thread.currentThread().getName() + Arrays.toString(aList.toArray()));
        },"thread2").start();
        for (int i = 0; i < 100 ; i++) {
            Thread.sleep(15);
            Student student = new Student();
            student.setName("thread1");
            KieSession session = getKieBase().newKieSession();
            session.insert(student);
            session.setGlobal("aList", aList);
            session.fireAllRules(new RuleNameEqualsAgendaFilter("rule_advanceKey_global_add_syn"));
            session.dispose();
        }
        System.out.println(Thread.currentThread().getName() + Arrays.toString(aList.toArray()));
    }

    /**
     * 测试LHS加强--in/not in
     */
    @Test
    public void testLhsImproveIn() {
        Student student = new Student();
        student.setName("zhangsan");
        executeRule(student, new RuleNameStartsWithAgendaFilter("rule_LHS_improve"));
        student.setName("张三");
        restartSession();
        executeRule(student, new RuleNameStartsWithAgendaFilter("rule_LHS_improve"));
    }

    /**
     * 测试LHS加强--eval
     */
    @Test
    public void testLhsImproveEval() {
        Student student = new Student();
        student.setName("张三");
        student.setAge(10);
        executeRule(student, new RuleNameStartsWithAgendaFilter("rule_LHS_improve"));
    }

    /**
     * 测试LHS加强--not
     */
    @Test
    public void testLhsImproveNot() {
        Student student = new Student();
        student.setName("张三not");
        executeRule(student, new RuleNameStartsWithAgendaFilter("rule_LHS_improve"));
    }

    /**
     * 测试LHS加强--exists
     */
    @Test
    public void testLhsImproveExists() {
        Student s = new Student();
        s.setName("张三exists");
        getKieSession().insert(s); // 单独插入一次,使工作内存中存在两条符合条件的Fact对象,验证exists和不加的区别
        Student student = new Student();
        student.setName("张三exists");
        executeRule(student, new RuleNameStartsWithAgendaFilter("rule_LHS_improve"));
    }

    /**
     * 测试LHS加强--extends
     */
    @Test
    public void testLhsImproveExtends() {
        Student student = new Student();
        student.setName("张三extends");
        student.setAge(11);
        executeRule(student, new RuleNameStartsWithAgendaFilter("rule_LHS_improve"));
    }

    /**
     * 测试RHS加强--halt
     */
    @Test
    public void testRhsImproveHalt() {
        executeRule(null, new RuleNameStartsWithAgendaFilter("rule_RHS_improve_halt"));
    }

    /**
     * 测试RHS加强--workingMemory
     */
    @Test
    public void testRhsImproveWorkingMemory() {
        executeRule(null, new RuleNameStartsWithAgendaFilter("rule_RHS_improve_working_memory"));
    }

    /**
     * 测试RHS加强--workingMemory
     */
    @Test
    public void testRhsImproveRule() {
        executeRule(null, new RuleNameStartsWithAgendaFilter("rule_RHS_improve_rule"));
    }


}
